var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var UserSchema = new Schema({
    facebookId: {
        type: String
    },
    name: {
        type: String
    },
    surname: {
        type: String
    },
    nickname: {
        type: String
    },
    email: {
        type: String
    },
    access_token: {
        type: String
    },
    createAt: {
        type: Date, default: Date.now
    },
});

var User = mongoose.model('User', UserSchema);
